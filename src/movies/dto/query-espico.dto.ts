import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsInt, IsOptional, IsString } from 'class-validator';

export class FilterEspicoDto {
  @IsOptional()
  @ApiPropertyOptional()
  @IsInt()
  espico_id: number;

  @ApiPropertyOptional()
  @IsInt()
  server: number;
}
