import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsInt, IsOptional, IsString } from 'class-validator';

export class filterDto {
  @IsOptional()
  @ApiPropertyOptional()
  @IsInt()
  skip: number;


  @IsOptional()
  @ApiPropertyOptional()
  @IsInt()
  take: number;

  @IsOptional()
  @ApiPropertyOptional()
  @IsString()
  year_movie: string;

  @IsOptional()
  @ApiPropertyOptional()
  @IsString()
  cast: string;

  @IsOptional()
  @ApiPropertyOptional()
  @IsString()
  national: string;
}
