import { ApiPropertyOptional, PartialType } from '@nestjs/swagger';
import { IsBoolean, isInt, IsInt, IsOptional, IsString } from 'class-validator';
import { CreateMovieDto } from './create-movie.dto';

export class UpdateMovieDto {
  @ApiPropertyOptional()
  @IsOptional()
  image: string;

  @ApiPropertyOptional()
  @IsOptional()
  description: string;

  @ApiPropertyOptional()
  @IsOptional()
  title: string;

  @ApiPropertyOptional()
  @IsOptional()
  url_embed: string;

  @ApiPropertyOptional()
  @IsOptional()
  status: boolean;

  @ApiPropertyOptional()
  @IsInt()
  cate: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsInt()
  like: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsInt()
  unlike: number;

  @ApiPropertyOptional()
  @IsOptional()
  slug: string;

  @ApiPropertyOptional()
  @IsInt()
  @IsOptional()
  evalute: number;

  @ApiPropertyOptional()
  @IsOptional()
  type: string

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  director: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  cast: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  time_movie: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  year_movie: string;
}
