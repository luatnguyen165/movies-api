import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsInt, IsOptional, IsString } from 'class-validator';

export class CreateMovieDto {
  @ApiProperty()
  @IsString()
  image_full: string;

  @ApiProperty()
  @IsString()
  description: string;

  @ApiProperty()
  @IsString()
  title: string;

  @ApiProperty()
  @IsString()
  image_small: string;

  @ApiProperty()
  @IsBoolean()
  status: boolean;

  @ApiProperty()
  @IsString()
  national: string;

  @ApiProperty()
  @IsInt()
  cate: number;


  @ApiProperty()
  @IsInt()
  @IsOptional()
  evalute: number;

  @ApiProperty()
  @IsOptional()
  type: string

  @ApiProperty()
  @IsOptional()
  @IsString()
  director: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  cast: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  time_movie: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  year_movie: string;

}
