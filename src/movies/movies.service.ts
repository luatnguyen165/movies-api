import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryService } from 'src/category/category.service';
// import { CategoryEnity } from 'src/category/entities/category.entity';
import { Repository } from 'typeorm';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { MoviesEnitity } from './entities/movie.entity';
import * as _ from 'lodash';
// import * as moment from 'moment';
import slugify from 'slugify';
@Injectable()
export class MoviesService {
  constructor(
    @InjectRepository(MoviesEnitity)
    private readonly moveRepository: Repository<MoviesEnitity>,
    private readonly cateRepository: CategoryService,
  ) {}
  async getMovieById(id, filterEspicoDto) {
    try {
      const movie = await this.moveRepository.findOne({
        where: { id },
        relations: ['cate', 'episode'],
      });

      await this.moveRepository.update(
        { id },
        {
          view: _.get(movie, 'view', 0) + 1,
        },
      );
      let movieItem;
      let espicode;
      let server;
      if (_.get(filterEspicoDto, 'espico_id', false) !== false) {
        movieItem = _.get(movie, 'episode', []).filter(
          (v) => v.id == _.get(filterEspicoDto, 'espico_id', ''),
        );
        espicode = _.get(movie, 'episode', []).map((v) => {
          if (v.id == filterEspicoDto.espico_id) {
            return {
              ...v,
              isActive: true,
            };
          }
          return {
            ...v,
            isActive: false,
          };
        });
        delete movie.episode;
        switch (_.toNumber(filterEspicoDto.server)) {
          case 1:
            server = _.get(movieItem[0], 'server_one', '');
            break;
          case 2:
            server = _.get(movieItem[0], 'server_two', '');
            break;
          case 3:
            server = _.get(movieItem[0], 'server_three', '');
            break;
          default:
            server = _.get(movieItem[0], 'server_one', '');
            break;
        }
        delete movieItem[0].server_one;
        delete movieItem[0].server_two;
        delete movieItem[0].server_three;
        movieItem = { ...movieItem[0], server };
        return {
          status: HttpStatus.OK,
          message: 'Lấy chi tiết movies thành công',
          data: {
            ...movie,
            espicode,
            movieItem,
          },
        };
      }
    } catch (error) {}
  }
  async create(createMovieDto: CreateMovieDto) {
    try {
      const cate = await this.cateRepository.getById(createMovieDto.cate);

      const movesNew = this.moveRepository.create({
        ...createMovieDto,
        slug: _.toUpper(createMovieDto.title),
        cate,
        createdAt: new Date(),
      });
      await this.moveRepository.save(movesNew);
      return {
        status: HttpStatus.CREATED,
        message: 'Tạo Phim mới thành công',
      };
    } catch (error) {
      throw new HttpException(
        'INTERAL ERROR',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async findAll(filterDto) {
    const whereObj = {};
    if (_.get(filterDto, 'year_movie', false) !== false) {
      _.set(whereObj, 'year_movie', _.get(filterDto, 'year_movie', ''));
    }
    if (_.get(filterDto, 'national', false) !== false) {
      _.set(whereObj, 'national', _.get(filterDto, 'national', ''));
    }
    if (_.get(filterDto, 'cast', false) !== false) {
      _.set(whereObj, 'cast', _.get(filterDto, 'cast', ''));
    }
    const data = await this.moveRepository.find({
      relations: ['cate', 'episode'],
      take: filterDto.take || 8,
      where: whereObj,
      skip: filterDto.skip || 0,
    });
    const dataCheck = _.map(data, (v) => {
      if (v.episode.length > 0) {
        return {
          ...v,
        };
      }
    });
    return {
      message: 'Lấy danh sách phim thành công',
      status: HttpStatus.OK,
      data: dataCheck.filter((v) => v !== undefined),
    };
  }

  async update(id: number, updateMovieDto: UpdateMovieDto) {
    try {
      const exist = await this.moveRepository.findOne({
        where: { id },
      });
      if (!exist) {
        return {
          status: HttpStatus.BAD_GATEWAY,
          message: `${id} Không tìm thấy `,
        };
      }
      if (_.get(updateMovieDto, 'title', false) !== false) {
        const existTitle = await this.moveRepository.findOne({
          where: {
            title: updateMovieDto.title.toUpperCase(),
          },
        });
        if (existTitle) {
          return {
            status: HttpStatus.BAD_REQUEST,
            message: 'Tiêu đề danh mục đã tồn tại',
          };
        }
        updateMovieDto.slug = updateMovieDto.title
          ? _.toLower(slugify(updateMovieDto.title))
          : existTitle.slug;
        updateMovieDto.title = updateMovieDto.title
          ? _.toUpper(updateMovieDto.title)
          : existTitle.title;
      }
      const cate = await this.cateRepository.getById(updateMovieDto.cate);
      updateMovieDto.unlike = updateMovieDto.unlike
        ? _.get(exist, 'unlike', 0) + 1
        : _.get(exist, 'unlike', 0);
      updateMovieDto.like = updateMovieDto.like
        ? _.get(exist, 'like', 0) + 1
        : _.get(exist, 'like', 0);
      const update = await this.moveRepository.update(
        { id },
        {
          ...updateMovieDto,
          cate: cate,
        },
      );
      if (!update || update.affected < 1) {
        return {
          status: HttpStatus.BAD_REQUEST,
          message: 'Cập nhật thất bại',
        };
      }
      return {
        status: HttpStatus.OK,
        message: 'Cập nhật thành công',
      };
    } catch (error) {
      console.log(error);

      throw new HttpException(
        'INTERNAL ERROR',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async remove(id: number) {
    try {
      const exist = await this.moveRepository.findOne({
        where: { id },
      });
      if (!exist) {
        return {
          status: HttpStatus.BAD_GATEWAY,
          message: `${id} Không tìm thấy `,
        };
      }
      await this.moveRepository.delete(id);
      return {
        status: HttpStatus.OK,
        message: 'Xoá phim  thành công',
      };
    } catch (error) {
      console.log(error);

      throw new HttpException(
        'INTERNAL ERROR',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  async findById(id: number) {
    return await this.moveRepository.findOne({
      where: {
        id,
      },
    });
  }
}
