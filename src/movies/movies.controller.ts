import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ValidationPipe,
  Put,
  ParseIntPipe,
  Query,
  ParseBoolPipe,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { MoviesService } from './movies.service';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { filterDto } from './dto/query-movie.dto';
import { FileFieldsInterceptor } from '@nestjs/platform-express/multer';
import { FilterEspicoDto } from './dto/query-espico.dto';

@Controller('movies')
@ApiTags('movies')
export class MoviesController {
  constructor(private readonly moviesService: MoviesService) {}

  @Post('create-movie')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        description: { type: 'string' },
        title: { type: 'string' },
        status: { type: 'boolean' },
        national: { type: 'string' },
        cate: { type: 'number' },
        evalute: { type: 'number' },
        type: { type: 'string' },
        director: { type: 'string' },
        cast: { type: 'string' },
        time_movie: { type: 'string' },
        year_movie: { type: 'string' },
        image_full: {
          type: 'string',
          format: 'binary',
        },
        image_small: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'image_small', maxCount: 1 },
      { name: 'image_full', maxCount: 1 },
    ]),
  )
  create(
    @Body(new ValidationPipe()) createMovieDto: CreateMovieDto,
    @UploadedFiles()
    files: {
      image_small?: Express.Multer.File[];
      image_full?: Express.Multer.File[];
    },
  ) {
    console.log(files);

    return this.moviesService.create({ ...createMovieDto });
  }
  @Get(':id/watch')
  async getMovie(
    @Param('id', ParseIntPipe) id: number,
    @Query() filterDto: FilterEspicoDto,
  ) {
    return this.moviesService.getMovieById(id, filterDto);
  }

  @Get()
  findAll(@Query() FilterDto: filterDto) {
    return this.moviesService.findAll(FilterDto);
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body(new ValidationPipe()) updateMovieDto: UpdateMovieDto,
  ) {
    return this.moviesService.update(+id, updateMovieDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.moviesService.remove(+id);
  }
}
