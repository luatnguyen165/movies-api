import { CategoryEnity } from 'src/category/entities/category.entity';
import { BaseEntity } from 'src/common/common.entity';
import { EpisodeEntity } from 'src/episode/entities/episode.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'movies' })
export class MoviesEnitity extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  // tên phim
  @Column()
  title: string;
  // slug
  @Column()
  slug: string;

  // đánh giá
  @Column({
    default: 0
  })
  evalute: number;
  // nội dụng
  @Column()
  description: string;
  // view
  @Column({
    default: 0
  })
  view: number;
  // đang phát hành
  @Column()
  type: string;

  // đạo diễn
  @Column()
  director: string;

  // diễn viên
  @Column()
  cast: string;

  // hình ảnh lớn
  @Column()
  image_full: string;

  // hình ảnh nhỏ
  @Column()
  image_small: string;

  // trạng thái
  @Column({
    default: false,
  })
  status: boolean;

  // lượt thích
  @Column({
    default: 0
  })
  like: number;

  // lượt k thích
  @Column({
    default: 0
  })
  unlike: number;

  // quốc gia
  @Column()
  national: string;

  @Column()
  time_movie: string;

  @Column()
  year_movie: string;

  @ManyToOne(() => CategoryEnity, (cate) => cate.movies)
  cate: CategoryEnity;
  @OneToMany(() => EpisodeEntity, (episode) => episode.movie)
  episode: EpisodeEntity[];
}
