import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { CategoryModule } from './category/category.module';
import { TypeOrmModule } from '@nestjs/typeorm';

import { LoggerModule } from 'nestjs-pino';
import { ConfigModule } from '@nestjs/config';
import { MoviesModule } from './movies/movies.module';
import { EpisodeModule } from './episode/episode.module';
import { UsersModule } from './users/users.module';
import { ProfileModule } from './profile/profile.module';
import { join } from 'path';
// import { AuthService } from './auth/auth.service';
// import { AuthModule } from './auth/auth.module';
import * as _ from 'lodash';
// import { LoggerMiddlewareTsMiddleware } from './middleware/logger.middleware.ts.middleware';
import { AuthModule } from './auth/auth.module';
import { ThrottlerModule } from '@nestjs/throttler';
// import { RolesModule } from './roles/roles.module';
import { TelegrafModule } from 'nestjs-telegraf';
import { AppService } from './app.service';
import { AppGateway } from './app.gateway';
import { ServeStaticModule } from '@nestjs/serve-static';
// import { join } from 'path';
@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'static'),
    }),
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 10,
    }),
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TelegrafModule.forRoot({
      token: process.env.TELEGRAM_BOT_TOKEN,
    }),
    // LoggerModule.forRoot({
    //   pinoHttp: {
    //     transport: {
    //       target: 'pino-pretty',
    //       options: {
    //         singleLine: true,
    //       },
    //     },
    //   },
    // }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DATABASE_HOST,

      port: _.toNumber(process.env.DATABASE_DEV_PORT),

      username: process.env.DATABASE_USERNAME,

      password: process.env.DATABASE_PASSWORD,

      database: process.env.DATABASE_DB,

      entities: [join(__dirname, '**', '*.entity.{ts,js}')],
      synchronize: process.env.NODE_ENV !== 'dev' ? true : false,
      autoLoadEntities: process.env.NODE_ENV !== 'dev' ? true : false,
    }),
    CategoryModule,
    MoviesModule,
    EpisodeModule,
    UsersModule,
    ProfileModule,
    AuthModule,

    // RolesModule,
    // BotModule,
    // AuthModule,
  ],
  controllers: [],
  providers: [AppService, AppGateway],
})
export class AppModule {}
// export class AppModule {}
