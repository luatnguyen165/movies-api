import { BaseEntity } from 'src/common/common.entity';
// import { UserEnity } from 'src/users/entities/user.entity';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'profile_user' })
export class ProfileEnity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({
    type: 'enum',
    enum: ['male', 'female', 'other'],
    default: 'other',
  })
  sex: string;

  @Column()
  phone: string;

  @Column()
  avatar: string;

  @Column()
  alias: string;
}
