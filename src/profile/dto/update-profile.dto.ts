import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';


export class UpdateProfileDto {
    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    firstName: string;
  
    @ApiPropertyOptional()
    @IsString()
    @IsOptional()
    lastName: string;
  
    @ApiPropertyOptional()
    @IsString()
    @IsOptional()
    sex: string;
  
    @ApiPropertyOptional()
    @IsString()
    @IsOptional()
    phone: string;
  
    @IsString()
    @IsOptional()
    avatar: string;
  
    @IsString()
    @IsOptional()
    alias: string;
}
