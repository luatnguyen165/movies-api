import { Module } from '@nestjs/common';
import { ProfileService } from './profile.service';
import { ProfileController } from './profile.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProfileEnity } from './entities/profile.entity';
import { UsersModule } from 'src/users/users.module';
// import { UsersService } from './../users/users.service';
import { UserEnity } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ProfileEnity, UserEnity]), UsersModule],
  controllers: [ProfileController],
  providers: [ProfileService],
})
export class ProfileModule {}
