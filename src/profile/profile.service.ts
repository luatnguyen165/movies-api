import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProfileDto } from './dto/create-profile.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { ProfileEnity } from './entities/profile.entity';
import { UsersService } from './../users/users.service';
import { UserEnity } from 'src/users/entities/user.entity';
import * as _ from 'lodash';
@Injectable()
export class ProfileService {
  constructor(
    @InjectRepository(ProfileEnity)
    private readonly profileService: Repository<ProfileEnity>,
    @InjectRepository(UserEnity)
    private readonly usersService: Repository<UserEnity>,
  ) {}
  async create(id, createProfileDto: CreateProfileDto) {
    const profile = await this.usersService.findOne({
      where: { id: id },
    });
    if (!profile)
      throw new HttpException(`${id} not found user`, HttpStatus.BAD_REQUEST);
    const profileData = await this.profileService.create({
      ...createProfileDto,
    });
    const saveData = await this.profileService.save(profileData);
    profile.profile = saveData;
    await this.usersService.save(profile);
    return {
      status: HttpStatus.CREATED,
      message: 'Cập nhật thông tin thành công',
    };
  }
  async updateProfile(id: number, updateProfile: any) {
    try {
      const profile = await this.usersService.findOne({
        where: { id: id },
        relations: ['profile'],
      });
      if (!profile)
        throw new HttpException(
          `${id} Không tìm thấy tài khoản`,
          HttpStatus.BAD_REQUEST,
        );
      const update = await this.profileService.update(
        { id: _.get(profile, 'profile.id', '') },
        updateProfile,
      );
      if (!update || update.affected < 1) {
        return {
          status: HttpStatus.BAD_REQUEST,
          message: 'Cập nhật thông tin thất bại',
        };
      }
      return {
        status: HttpStatus.OK,
        message: 'Cập nhật thông tin thành công',
      };
    } catch (error) {}
  }

  async profile(id: number) {
    const profile = await this.usersService.findOne({
      where: { id: id },
      relations: ['profile'],
    });
    if (!profile)
      throw new HttpException(
        `${id} Không tìm thấy tài khoản`,
        HttpStatus.BAD_REQUEST,
      );

    const profileUser = _.get(profile, 'profile', {});
    if (profileUser) {
      return {
        status: HttpStatus.OK,
        message: 'Lấy thông tin thành công',
        data: profileUser,
      };
    }
    return {
      status: HttpStatus.OK,
      message: 'Thông tin chưa cập nhật vui lòng cập nhật thông tin',
      data: {},
    };
  }
}
