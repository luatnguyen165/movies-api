import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Request,
  HttpStatus,
  Put,
} from '@nestjs/common';
import { ProfileService } from './profile.service';
import { CreateProfileDto } from './dto/create-profile.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { ApiTags } from '@nestjs/swagger';
// import { UpdateProfileDto } from './dto/update-profile.dto';

@Controller('profile')
@ApiTags('profile')
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @Post('profile-user')
  @UseGuards(JwtAuthGuard)
  create(@Request() req, @Body() createProfileDto: CreateProfileDto) {
    const { userId } = req.user;
    if (!userId)
      return {
        status: HttpStatus.BAD_REQUEST,
        message: 'Không tìm thấy tài khoản',
      };
    return this.profileService.create(userId, createProfileDto);
  }
  @Get('profile-user')
  @UseGuards(JwtAuthGuard)
  getProfile(@Request() req) {
    const { userId } = req.user;
    if (!userId)
      return {
        status: HttpStatus.BAD_REQUEST,
        message: 'Không tìm thấy tài khoản',
      };
    return this.profileService.profile(userId);
  }
  @Put('profile-user')
  @UseGuards(JwtAuthGuard)
  updateProfile(@Request() req, @Body() updateDto: UpdateProfileDto) {
    const { userId } = req.user;
    // console.log('user: ' + userId);
    
    if (!userId)
      return {
        status: HttpStatus.BAD_REQUEST,
        message: 'Không tìm thấy tài khoản',
      };
    return this.profileService.updateProfile(userId, updateDto);
  }
}
