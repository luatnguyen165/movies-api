import { Column, Entity } from 'typeorm';

export abstract class BaseEntity {
  @Column({
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
    type: 'datetime',
  })
  createdAt: Date;
  @Column({
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
    type: 'datetime',
  })
  updatedAt: Date;
}
