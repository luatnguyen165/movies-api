import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import compression from '@fastify/compress';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
// import { Logger, LoggerErrorInterceptor } from 'nestjs-pino';
import { AppModule } from './app.module';
import { join } from 'path';
// import fastifyMultipart from 'fastify-multipart';
async function bootstrap() {
  // const app = await NestFactory.create(AppModule,{
  //   bufferLogs: true,
  // });
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
    {
      bufferLogs: true,
    },
  );
  // app.register(fastifyMultipart, {
  //   limits: {
  //     fieldNameSize: 100, // Max field name size in bytes
  //     fieldSize: 1000000, // Max field value size in bytes
  //     fields: 10, // Max number of non-file fields
  //     fileSize: 100, // For multipart forms, the max file size
  //     files: 1, // Max number of file fields
  //     headerPairs: 2000, // Max number of header key=>value pairs
  //   },
  // });
  const config = new DocumentBuilder()
    .setTitle('Movies api')
    .setDescription('This is Movies API documentation')
    .setVersion('1.0')
    .addTag('movie')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  app.enableCors();
  // app.useLogger(app.get(Logger));
  await app.register(compression, { encodings: ['gzip', 'deflate'] });
  // app.useGlobalInterceptors(new LoggerErrorInterceptor());

  await app.listen(process.env.PORT || 3000, '0.0.0.0');
}
bootstrap();
