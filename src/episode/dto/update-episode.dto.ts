import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsInt, IsOptional, IsString } from 'class-validator';
export class UpdateEpisodeDto {
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  server_one: string;

  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  server_two: string;
  
  @ApiPropertyOptional()
  @IsString()
  @IsOptional()
  server_three: string;

  @IsInt()
  @IsOptional()
  @ApiPropertyOptional()
  episode_title: number;

  @ApiPropertyOptional()
  @IsBoolean()
  @IsOptional()
  status: boolean;

  @ApiProperty()
  @IsInt()
  movie: number;
}
