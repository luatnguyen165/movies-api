import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsInt, IsOptional, IsString } from 'class-validator';

export class CreateEpisodeDto {
  @ApiProperty()
  @IsString()
  server_one: string;

  @ApiPropertyOptional()
  @IsString()
  server_two: string;
  
  @ApiPropertyOptional()
  @IsString()
  server_three: string;

  @IsInt()
  @ApiProperty()
  episode_title: number;

  @ApiPropertyOptional()
  @IsBoolean()
  @IsOptional()
  status: boolean;

  @ApiProperty()
  @IsInt()
  movie: number;
}
