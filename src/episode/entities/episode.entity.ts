import { BaseEntity } from 'src/common/common.entity';
import { MoviesEnitity } from 'src/movies/entities/movie.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
  name: 'episode_movies',
})
export class EpisodeEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  server_one: string;

  @Column()
  server_two: string;

  @Column()
  server_three: string;

  @Column()
  episode_title: number;

  @Column({
    default: false,
  })
  status: boolean;

  @ManyToOne(() => MoviesEnitity, (movie) => movie.episode)
  movie: MoviesEnitity;
}
