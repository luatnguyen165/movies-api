import { HttpStatus, Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEpisodeDto } from './dto/create-episode.dto';
import { UpdateEpisodeDto } from './dto/update-episode.dto';
import { EpisodeEntity } from './entities/episode.entity';
import { MoviesService } from './../movies/movies.service';
import * as moment from 'moment';

@Injectable()
export class EpisodeService {
  constructor(
    @InjectRepository(EpisodeEntity)
    private readonly episodeRepository: Repository<EpisodeEntity>,
    private readonly moviesService: MoviesService,
  ) {}
  async create(createEpisodeDto: CreateEpisodeDto) {
    try {
      // console.log('Đã vào');
      const movie = await this.moviesService.findById(createEpisodeDto.movie);
      if (!movie)
        throw new HttpException(
          `${createEpisodeDto.movie} Không tìm thấy`,
          HttpStatus.BAD_REQUEST,
        );
      const created = await this.episodeRepository.create({
        ...createEpisodeDto,
        movie,
        createdAt: new Date()
      });
      const createdEpiosde = await this.episodeRepository.save(created);
      // console.log(createdEpiosde);
      if (createdEpiosde){
        return {
          status: HttpStatus.CREATED,
          message: 'Thêm tập phim thành công',
        };
      }
      return {
        status: HttpStatus.BAD_REQUEST,
        message: 'Thêm tập phim thất bại',
      };
    } catch (error) {
      console.error(error)
    }
  }

  async findAll() {
    const episodeList = await this.episodeRepository.find({
      relations: ['movie'],
    });
    return {
      status: HttpStatus.OK,
      message: 'Lấy danh sách tập phim thành công',
      data: episodeList,
    };
  }
  async update(id: number, updateEpisodeDto: UpdateEpisodeDto) {
    try {
      const episode = await this.episodeRepository.findOne({
        where: { id: id },
      });
      if (!episode)
        throw new HttpException(`${id} Không tìm thấy`, HttpStatus.BAD_REQUEST);
      const movie = await this.moviesService.findById(updateEpisodeDto.movie);
      if (!movie)
        throw new HttpException(
          `${updateEpisodeDto.movie} Không tìm thấy`,
          HttpStatus.BAD_REQUEST,
        );
      const created = this.episodeRepository.create({
        ...updateEpisodeDto,
        movie,
        updatedAt: new Date(),
      });
      await this.episodeRepository.save(created);
      return {
        status: HttpStatus.CREATED,
        message: 'Cập nhật tập phim thành công',
      };
    } catch (error) {}
  }

  async remove(id: number) {
    try {
      const episode = await this.episodeRepository.findOne({
        where: { id: id },
      });
      if (!episode)
        throw new HttpException(`${id} Không tìm thấy`, HttpStatus.BAD_REQUEST);

      await this.episodeRepository.delete(id);
      return {
        status: HttpStatus.OK,
        message: 'Xoá thành công tập phim',
      };
    } catch (error) {}
  }
}
