import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ValidationPipe,
  Put,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CategoryService } from './category.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { PagingDTO } from './dto/paging-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@ApiTags('Categories')
@Controller('categories')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @Get('list-category')
  async getCategory(@Query() data: PagingDTO) {
    return this.categoryService.getCategory(data);
  }
  @Post('create-category')
  async createPost(@Body(new ValidationPipe()) category: CreateCategoryDto) {
    return this.categoryService.postNewCategory(category);
  }

  @Put('update-category/:id')
  async updateCategory(
    @Body(new ValidationPipe()) updateCategory: UpdateCategoryDto,
    @Param('id', ParseIntPipe) id: number,
  ) {
    return this.categoryService.updateCategory(id, updateCategory);
  }
  @Delete('delete-category/:id')
  async deleteCategory(@Param('id', ParseIntPipe) id: number) {
    return this.categoryService.deleteCategory(id);
  }
}
