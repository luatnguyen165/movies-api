import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsInt, IsOptional, IsString } from 'class-validator';
export class PagingDTO {
  @ApiPropertyOptional()
  @IsOptional()
  @IsInt()
  skip: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsInt()
  take: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  keyword: string;
}
