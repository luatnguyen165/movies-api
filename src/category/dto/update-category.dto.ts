import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsInt,
  IsOptional,
  IsBoolean,
  IsSemVer,
  IsString,
} from 'class-validator';

export class UpdateCategoryDto {
  @ApiPropertyOptional()
  @IsOptional()
  title: string;

  @ApiPropertyOptional()
  @IsOptional()
  description: string;

  @IsOptional()
  @IsString()
  slug: string;
  @IsOptional()
  @IsBoolean()
  status: boolean;
}
