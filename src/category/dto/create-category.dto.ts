import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
export class CreateCategoryDto {
  @IsString()
  @ApiProperty({
    description: 'Tiêu đề danh mục'
  })
  title: string;

  @ApiProperty({
    description: 'Nội dung danh mục'
  })
  @IsString()
  description: string;

  // @IsOptional()
  // slug: string;
  @IsOptional()
  @IsBoolean()
  status: boolean;
}
