import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { CategoryEnity } from './entities/category.entity';
import { UpdateCategoryDto } from './dto/update-category.dto';
import * as _ from 'lodash';
import * as moment from 'moment';
import slugify from 'slugify';
@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(CategoryEnity)
    private readonly categoryRepository: Repository<CategoryEnity>,
  ) {}
  private readonly logger = new Logger(CategoryService.name);
  async getCategory(data: any) {
    try {
      const take = data.take || 10;
      const skip = data.skip || 0;
      if (_.isEmpty(data.keyword)) {
        const [categoryList, total] =
          await this.categoryRepository.findAndCount({
            relations:['movies'],
            take: take,
            skip: skip,
            order: {
              id: 'DESC',
            },
          });
        return {
          status: HttpStatus.OK,
          message: 'Lấy danh sách danh mục thành công',
          data: categoryList,
          total,
        };
      }
      const [categoryList, total] = await this.categoryRepository.findAndCount({
        where: {
          slug: Like('%' + _.toLower(slugify(data.keyword)) + '%'),
        },
        relations:['movies'],
        take: take,
        skip: skip,
        order: {
          id: 'DESC',
        },
      });
      if (!categoryList || !_.isArray(categoryList)) {
        return {
          status: HttpStatus.BAD_GATEWAY,
          message: 'Lấy danh sách danh mục thất bại',
        };
      }
      return {
        status: HttpStatus.OK,
        message: 'Lấy danh sách danh mục thành công',
        data: categoryList,
        total,
      };
    } catch (error) {
      this.logger.error('[GET LIST CATEGORY ERROR]', JSON.stringify(error));
      throw new HttpException(
        'INTERNAL ERROR',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  async postNewCategory(category: CreateCategoryDto) {
    try {
      category.title = category.title.toUpperCase();
      const existTitle = await this.categoryRepository.findOne({
        where: {
          title: category.title.toUpperCase(),
        },
        relations:['movies'],
      });
      if (existTitle) {
        return {
          status: HttpStatus.BAD_REQUEST,
          message: 'Tiêu đề danh mục đã tồn tại',
        };
      }
      const newCategory = this.categoryRepository.create({
        ...category,
        slug: slugify(category.title),
        createdAt: new Date(),
      });
      const created = await this.categoryRepository.save(newCategory);

      if (!created || !created.id) {
        return {
          status: HttpStatus.BAD_REQUEST,
          message: 'Tạo danh mục thất bại',
        };
      }
      return {
        status: HttpStatus.CREATED,
        message: 'Tạo danh mục thành công',
      };
    } catch (error) {
      this.logger.log('[ERROR]', error)
      throw new HttpException(
        error,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  async updateCategory(id, updateData: UpdateCategoryDto) {
    try {
      const exist = await this.categoryRepository.findOne({
        where: { id },
      });
      if (!exist) {
        return {
          status: HttpStatus.BAD_GATEWAY,
          message: `${id} Không tìm thấy `,
        };
      }
      if (_.get(updateData, 'title', false) !== false) {
        const existTitle = await this.categoryRepository.findOne({
          where: {
            title: updateData.title.toUpperCase(),
          },
        });
        if (existTitle) {
          return {
            status: HttpStatus.BAD_REQUEST,
            message: 'Tiêu đề danh mục đã tồn tại',
          };
        }
        updateData.slug = updateData.title
          ? _.toLower(slugify(updateData.title))
          : existTitle.slug;
        updateData.title = updateData.title
          ? _.toUpper(updateData.title)
          : existTitle.title;
      }

      const update = await this.categoryRepository.update(
        { id },
        { ...updateData, updatedAt: new Date() },
      );
      if (!update || update.affected < 1) {
        return {
          status: HttpStatus.BAD_REQUEST,
          message: 'Cập nhật thất bại',
        };
      }
      return {
        status: HttpStatus.OK,
        message: 'Cập nhật thành công',
      };
    } catch (error) {
      console.log(error);

      throw new HttpException(
        'INTERNAL ERROR',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  async deleteCategory(id) {
    try {
      const exist = await this.categoryRepository.findOne({
        where: { id },
      });
      if (!exist) {
        return {
          status: HttpStatus.BAD_GATEWAY,
          message: `${id} Không tìm thấy `,
        };
      }
      await this.categoryRepository.delete(id);
      return {
        status: HttpStatus.OK,
        message: 'Xoá danh mục thành công',
      };
    } catch (error) {
      console.log(error);

      throw new HttpException(
        'INTERNAL ERROR',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  async getById(id: number) {
    return this.categoryRepository.findOne({
      where: {
        id,
      },
    });
  }
}
