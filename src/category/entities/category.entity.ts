import { BaseEntity } from 'src/common/common.entity';
import { MoviesEnitity } from 'src/movies/entities/movie.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'category_movies' })
export class CategoryEnity extends BaseEntity{
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  slug: string;


  @Column({ default: false })
  status: boolean;

  @OneToMany(() => MoviesEnitity, (movie) => movie.cate)
  movies: MoviesEnitity[];
}
