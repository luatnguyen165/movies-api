import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { CreateAuthDto } from './dto/create-auth.dto';
import { loginDTO } from './dto/update-auth.dto';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt/dist';
@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly JwtService: JwtService,
  ) {}
  async register(createUser: any) {
    try {
      const user = await this.usersService.createService(createUser);
      return { ...user };
    } catch (error) {}
  }

  async login(userDto: loginDTO) {
    const user = await this.usersService.loginService(userDto);
    if (!user)
      throw new HttpException(
        'Tài khoản không tồn tại',
        HttpStatus.BAD_GATEWAY,
      );
    const payload = {
      email: user.email,
      id: user.id,
      role: user.role
    };
    const token = await this._createToken(payload);
    if (!token)
      throw new HttpException('Invalid token', HttpStatus.BAD_REQUEST);
    return {
      status: 'Đăng nhập thành công',
      token
    };
  }
  private async _createToken(payload) {
    return await this.JwtService.sign(payload, {
      secret: process.env.DEV_SECRET,
      expiresIn: process.env.EXPIRED_TIME 
    });
  }
}
