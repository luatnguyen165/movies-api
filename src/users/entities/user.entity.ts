import { BaseEntity } from 'src/common/common.entity';
import { ProfileEnity } from 'src/profile/entities/profile.entity';
// import { Roles } from 'src/roles/entities/role.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
export enum UserRole {
  ADMIN = 'ADMIN',
  USER = 'USER',
  OPERATION = 'OPERATION',
}
@Entity({ name: 'user' })
export class UserEnity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: [UserRole.USER],
  })
  role: UserRole[];
  @OneToOne(() => ProfileEnity)
  @JoinColumn()
  profile: ProfileEnity;
  // registerAsUser: Promise<Roles>;
}
