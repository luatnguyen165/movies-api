import { HttpStatus, Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEnity } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
// import { Roles } from 'src/roles/entities/role.entity';
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEnity)
    private readonly userRepository: Repository<UserEnity>
  ) {}
  async createService(createUserDto: CreateUserDto) {
    const userFind = await this.userRepository.findOne({
      where: {
        email: createUserDto.email,
      },
    });
    if (userFind) {
      return {
        status: HttpStatus.BAD_REQUEST,
        message: 'Email đã tồn tại trong hệ thống ',
      };
    }
    if (createUserDto.password !== createUserDto.re_password) {
      return {
        status: HttpStatus.BAD_REQUEST,
        message: 'Mật khẩu đăng nhập không khớp vui lòng kiểm tra lại ',
      };
    }
    const saltOrRounds = 10;
    const password = createUserDto.password;
    createUserDto.password = await bcrypt.hash(password, saltOrRounds);
    delete createUserDto.re_password;
    const registerAsUser = await this.userRepository.create(createUserDto);
    await this.userRepository.save(registerAsUser);
    return {
      status: HttpStatus.OK,
      message: 'Đăng ký thành công',
    };
  }

  async loginService(updateUserDto: UpdateUserDto) {
    try {
      const user = await this.userRepository.findOne({
        where: { email: updateUserDto.email },
      });

      if (!user)
        throw new HttpException(
          'Tài khoản hiện tại không tồn tại',
          HttpStatus.NOT_FOUND,
        );
      const match = await bcrypt.compare(updateUserDto.password, user.password);
      if (!match)
        throw new HttpException(
          'Tài khoản hiện tại không đúng',
          HttpStatus.BAD_REQUEST,
        );
      return user;
    } catch (error) {
      console.error(error);
    }
  }
  async getById(id: number) {
    return await this.userRepository.findOne({
      where: {
        id,
      },
    });
  }
}
