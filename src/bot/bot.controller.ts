import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpStatus,
  Res,
} from '@nestjs/common';
import { BotService } from './bot.service';

@Controller()
export class BotController {
  constructor(private botService: BotService) {}
  @Get()
  getBotDialog(@Res() res) {
    this.botService.botMessage();
    res.status(HttpStatus.OK).send('Bot service started');
  }
}
