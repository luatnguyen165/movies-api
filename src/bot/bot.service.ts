// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { OnModuleInit } from '@nestjs/common';
import TelegramBot from 'node-telegram-bot-api';
// @Component()
export class BotService implements OnModuleInit {
  onModuleInit() {
    this.botMessage();
}
  botMessage() {

    process.env.NTBA_FIX_319 = '1';

    const token = 'YOUR_ACCESS_TOKEN';

    const bot = new TelegramBot(token, { polling: true });

    bot.on('message', (msg) => {
      const Hi = 'hi';
      if (msg.text.toString().toLowerCase().indexOf(Hi) === 0) {
        bot.sendMessage(
          msg.from.id,
          'Hello ' +
            msg.from.first_name +
            ' what would you like to know about me ?',
        );
      }
    });
  }
}
